# componance-bottom-sheet

[![CI Status](https://img.shields.io/travis/Pimmii-Dev/componance-bottom-sheet.svg?style=flat)](https://travis-ci.org/Pimmii-Dev/componance-bottom-sheet)
[![Version](https://img.shields.io/cocoapods/v/componance-bottom-sheet.svg?style=flat)](https://cocoapods.org/pods/componance-bottom-sheet)
[![License](https://img.shields.io/cocoapods/l/componance-bottom-sheet.svg?style=flat)](https://cocoapods.org/pods/componance-bottom-sheet)
[![Platform](https://img.shields.io/cocoapods/p/componance-bottom-sheet.svg?style=flat)](https://cocoapods.org/pods/componance-bottom-sheet)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

componance-bottom-sheet is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'componance-bottom-sheet'
```

## Author

Pimmii-Dev, pimpun.dev@gmail.com

## License

componance-bottom-sheet is available under the MIT license. See the LICENSE file for more info.
