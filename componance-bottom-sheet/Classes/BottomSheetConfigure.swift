//
//  BottomSheetConfigure.swift
//  componance-bottom-sheet
//
//  Created by Pimpun Dev on 10/3/2564 BE.
//

import Foundation

open class BottomSheetConfigure: NSObject {
    public var contentSize: CGSize = UIScreen.main.bounds.size
    public var maxContentHeight: CGFloat = UIScreen.main.bounds.size.height
    public var paddingContentTop: CGFloat = 0
    public var activeBlurBackground: Bool = false
    public var activeTapDismiss: Bool = true
    public var activeFullSize: Bool = false
}
