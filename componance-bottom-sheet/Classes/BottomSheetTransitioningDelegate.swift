//
//  BottomSheetTransitioningDelegate.swift
//  componance-bottom-sheet
//
//  Created by Pimpun Dev on 17/2/2564 BE.
//

import Foundation

open class BottomSheetTransitioningDelegate: NSObject, UIViewControllerTransitioningDelegate {
    private var viewController: UIViewController
    private var presentingViewController: UIViewController
    private var interactionController: BottomSheetInteractiveTransition
    private var configure: BottomSheetConfigure
    
    var interactiveDismiss = true
    
    public init(viewController: UIViewController, presentingViewController: UIViewController, configure: BottomSheetConfigure = BottomSheetConfigure()) {
        self.viewController = viewController
        self.presentingViewController = presentingViewController
        self.configure = configure
        
        self.interactionController = BottomSheetInteractiveTransition(viewController: self.viewController, withView: self.presentingViewController.view, presentingViewController: self.presentingViewController)
        
        super.init()
    }
    
    public func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        interactiveDismiss = false
        return BottomSheetTransitionAnimator(type: .dismiss)
    }
    
    public func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        let presentation = BottomSheetPresentationController(presentedViewController: presented, presenting: presenting)
        presentation.configure = self.configure
        return presentation
    }
    
    public func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        if interactiveDismiss {
            return self.interactionController
        }
        
        return nil
    }
}
