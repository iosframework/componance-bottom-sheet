//
//  BottomSheetPresentable.swift
//  componance-bottom-sheet
//
//  Created by Pimpun Dev on 17/2/2564 BE.
//

import Foundation

public protocol BottomSheetPresentable { }

extension BottomSheetPresentable where Self: UIViewController {
    func maximizeToFullScreen() -> Void {
        if let presetation = navigationController?.presentationController as? BottomSheetPresentationController {
            presetation.changeScale(to: .adjustedOnce)
        }
    }
}

extension BottomSheetPresentable where Self: UINavigationController {
    func isHalfModalMaximized() -> Bool {
        if let presentationController = presentationController as? BottomSheetPresentationController {
            return presentationController.isMaximized
        }
        
        return false
    }
}
