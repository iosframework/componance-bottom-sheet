//
//  BottomSheetPresentationController.swift
//  componance-bottom-sheet
//
//  Created by Pimpun Dev on 17/2/2564 BE.
//

import Foundation

public enum ModalScaleState {
    case adjustedOnce
    case normal
}

open class BottomSheetPresentationController : UIPresentationController {
    var isMaximized: Bool = false
    var _dimmingView: UIView?
    var panGestureRecognizer: UIPanGestureRecognizer
    var direction: CGFloat = 0
    var state: ModalScaleState = .normal
    var configure: BottomSheetConfigure = BottomSheetConfigure()
    
    var dimmingView: UIView {
        if let dimmedView = _dimmingView {
            return dimmedView
        }
        
        let view = UIView(frame: CGRect(x: 0,
                                        y: 0,
                                        width: containerView!.bounds.width,
                                        height: containerView!.bounds.height))
        
        if configure.activeBlurBackground {
            // Blur Effect
            let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = view.bounds
            view.addSubview(blurEffectView)
            
            // Vibrancy Effect
            let vibrancyEffect = UIVibrancyEffect(blurEffect: blurEffect)
            let vibrancyEffectView = UIVisualEffectView(effect: vibrancyEffect)
            vibrancyEffectView.frame = view.bounds
            
            // Add the vibrancy view to the blur view
            blurEffectView.contentView.addSubview(vibrancyEffectView)
        }
        
        _dimmingView = view
        
        let tapDismiss = UITapGestureRecognizer(target: self, action: #selector(onTapDismiss(_:)))
        _dimmingView?.addGestureRecognizer(tapDismiss)
        
        return view
    }
    
    override init(presentedViewController: UIViewController, presenting presentingViewController: UIViewController?) {
        self.panGestureRecognizer = UIPanGestureRecognizer()
        super.init(presentedViewController: presentedViewController, presenting: presentingViewController)
        panGestureRecognizer.addTarget(self, action: #selector(onPan(pan:)))
        presentedViewController.view.addGestureRecognizer(panGestureRecognizer)
    }
    
    @objc func onPan(pan: UIPanGestureRecognizer) -> Void {
        let endPoint = pan.translation(in: pan.view?.superview)
        
        switch pan.state {
        case .began:
            presentedView!.frame.size.height = containerView!.frame.height
            
        case .changed:
            let velocity = pan.velocity(in: pan.view?.superview)
            print(velocity.y)
            switch state {
            case .normal:
                presentedView!.frame.origin.y = endPoint.y + self.configure.contentSize.height
            case .adjustedOnce:
                let space: CGFloat = configure.activeFullSize ? 0 : self.configure.contentSize.height
                presentedView!.frame.origin.y = endPoint.y + space
            }
            direction = velocity.y
            
            break
        case .ended:
            if configure.activeFullSize && direction > 0 && state == .normal {
                presentedViewController.dismiss(animated: true, completion: nil)
            } else {
                if direction < 0 {
                    changeScale(to: .adjustedOnce)
                } else {
                    if state == .adjustedOnce {
                        changeScale(to: .normal)
                    } else {
                        presentedViewController.dismiss(animated: true, completion: nil)
                    }
                }
            }
            
            print("finished transition")
        
        default:
            break
        }
    }
    
    @objc
    private func onTapDismiss(_ sender: UIPanGestureRecognizer) {
        guard configure.activeTapDismiss else { return }
        presentedViewController.dismiss(animated: true, completion: nil)
    }
    
    func changeScale(to state: ModalScaleState) {
        if let presentedView = presentedView, let containerView = self.containerView {
            UIView.animate(withDuration: 0.8,
                           delay: 0,
                           usingSpringWithDamping: 0.5,
                           initialSpringVelocity: 0.5,
                           options: .curveEaseInOut,
                           animations: { () -> Void in
                presentedView.frame = containerView.frame
                let containerFrame = containerView.frame
                            
                let fullFrame = CGRect(origin: CGPoint(x: 0, y: self.configure.paddingContentTop),
                                       size: CGSize(width: containerFrame.width, height: self.configure.maxContentHeight))
                
                let customFrame = CGRect(origin: CGPoint(x: 0, y: containerFrame.height - self.configure.contentSize.height),
                                         size: self.configure.contentSize)
                            
                let frame = state == .adjustedOnce && self.configure.activeFullSize ? fullFrame : customFrame
                
                presentedView.frame = frame
                
                if let navController = self.presentedViewController as? UINavigationController {
                    self.isMaximized = true
                    
                    navController.setNeedsStatusBarAppearanceUpdate()
                    
                    // Force the navigation bar to update its size
                    navController.isNavigationBarHidden = true
                    navController.isNavigationBarHidden = false
                }
            }, completion: { (isFinished) in
                self.state = state
            })
        }
    }
    
    open override var frameOfPresentedViewInContainerView: CGRect {
        return CGRect(origin: CGPoint(x: 0, y: containerView!.bounds.height - self.configure.contentSize.height),
                      size: self.configure.contentSize)
    }
    
    open override func presentationTransitionWillBegin() {
        let dimmedView = dimmingView
        
        if let containerView = self.containerView, let coordinator = presentingViewController.transitionCoordinator {
            
            dimmedView.alpha = 0
            containerView.addSubview(dimmedView)
            dimmedView.addSubview(presentedViewController.view)
            
            coordinator.animate(alongsideTransition: { (context) -> Void in
                dimmedView.alpha = 1
//                self.presentingViewController.view.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
            }, completion: nil)
        }
    }
    
    open override func dismissalTransitionWillBegin() {
        if let coordinator = presentingViewController.transitionCoordinator {
            coordinator.animate(alongsideTransition: { (context) -> Void in
                self.dimmingView.alpha = 0
//                self.presentingViewController.view.transform = CGAffineTransform.identity
            }, completion: { (completed) -> Void in
                print("done dismiss animation")
            })
        }
    }
    
    open override func dismissalTransitionDidEnd(_ completed: Bool) {
        print("dismissal did end: \(completed)")
        if completed {
            dimmingView.removeFromSuperview()
            _dimmingView = nil
            
            isMaximized = false
        }
    }
}
