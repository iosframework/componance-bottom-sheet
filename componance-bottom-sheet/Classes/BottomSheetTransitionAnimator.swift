//
//  BottomSheetTransitionAnimator.swift
//  componance-bottom-sheet
//
//  Created by Pimpun Dev on 17/2/2564 BE.
//

import Foundation

public enum BottomSheetTransitionAnimatorType {
    case present
    case dismiss
}

open class BottomSheetTransitionAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    var type: BottomSheetTransitionAnimatorType
    
    init(type:BottomSheetTransitionAnimatorType) {
        self.type = type
    }
    
    @objc public func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let _ = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)
        let from = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)
        
        UIView.animate(withDuration: transitionDuration(using: transitionContext), animations: { () -> Void in
            
            from!.view.frame.origin.y = 800
            
            print("animating...")
            
        }) { (completed) -> Void in
            print("animate completed")
            
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }
    }
    
    public func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.4
    }
}
