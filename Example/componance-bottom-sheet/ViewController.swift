//
//  ViewController.swift
//  componance-bottom-sheet
//
//  Created by Pimmii-Dev on 02/10/2021.
//  Copyright (c) 2021 Pimmii-Dev. All rights reserved.
//

import UIKit
import componance_bottom_sheet

class ViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onTouc() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let destinationVC = storyboard.instantiateViewController(withIdentifier: "DemoViewController")
        
        let _present = BottomSheetTransitioningDelegate(viewController: self, presentingViewController: destinationVC)

        destinationVC.modalPresentationStyle = .custom
        destinationVC.transitioningDelegate = _present

        present(destinationVC, animated: true, completion: nil)
    }
}

